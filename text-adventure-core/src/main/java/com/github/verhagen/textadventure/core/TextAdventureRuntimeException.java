package com.github.verhagen.textadventure.core;

public class TextAdventureRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TextAdventureRuntimeException(final String message) {
		super(message);
	}
	
}
