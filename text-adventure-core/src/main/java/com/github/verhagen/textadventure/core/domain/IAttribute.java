package com.github.verhagen.textadventure.core.domain;

public interface IAttribute<T> {

    String getName();
    
    T getValue();

}
